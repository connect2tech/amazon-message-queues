## Implementing Message Brokering with Amazon MQ By Michael Heydt
> https://bitbucket.org/connect2tech/amazon-message-queues  
> I need to come back to user level permission (on need basis)  
> Monitoring using CloudWatch

This diagram outlines the topics that are covered in the course.

![example-diagram](images/example-diagram.png)

### ~~Section 1: Course Overview~~

### ~~Section 2: Messaging with Amazon MQ~~

### ~~Section 3: Managing Amazon MQ~~


---

> awsmq/AmazonQueues12#$

[https://docs.aws.amazon.com/amazon-mq/latest/developer-guide/amazon-mq-creating-configuring-network-of-brokers.html](https://docs.aws.amazon.com/amazon-mq/latest/developer-guide/amazon-mq-creating-configuring-network-of-brokers.html)

```xml
<networkConnectors>
<networkConnector name="myNetworkConnector" 
    uri="masterslave:(ssl://b-1a2b3c4d-1.mq.region.amazonaws.com:61617,ssl://b-1a2b3c4d-2.mq.region.amazonaws.com:61617,ssl://b-fb036da0-d471-4915-9b96-765e74d843ff-1.mq.ap-southeast-1.amazonaws.com:61617)" 
    userName="awsmq"/>
</networkConnectors>
```


```xml
<networkConnectors>
    <networkConnector name="myNetworkConnector" uri="masterslave:(ssl://b-fb036da0-d471-4915-9b96-765e74d843ff-1.mq.ap-southeast-1.amazonaws.com:61617,ssl://b-fb036da0-d471-4915-9b96-765e74d843ff-1.mq.ap-southeast-1.amazonaws.com:61617)" userName="awsmq"/>
</networkConnectors>
```


---
> awsmq/Aws1Broker1aa3

[https://docs.aws.amazon.com/amazon-mq/latest/developer-guide/network-of-brokers.html](https://docs.aws.amazon.com/amazon-mq/latest/developer-guide/network-of-brokers.html)

```xml
<networkConnectors>
    <networkConnector name="myNetworkConnector1" userName="awsmq" uri="masterslave:(ssl://b-1a2b3c4d-1.mq.region.amazonaws.com:61617,ssl://b-1a2b3c4d-2.mq.region.amazonaws.com:61617,ssl://b-b649a908-73d7-4e1b-9000-4e7061579583-1.mq.ap-southeast-1.amazonaws.com:61617)"/>
    <networkConnector name="myNetworkConnector3" userName="awsmq" uri="masterslave:(ssl://b-1a2b3c4d-1.mq.region.amazonaws.com:61617,ssl://b-1a2b3c4d-2.mq.region.amazonaws.com:61617,ssl://b-4011737a-b8c6-49a2-956f-c4c9b50da23b-1.mq.ap-southeast-1.amazonaws.com:61617)"/>
</networkConnectors>
```

---

**Security Group**

![InboundRules](images/InboundRules.png)

![OutBoundRules](images/OutBoundRules.png)

---

## https://examples.javacodegeeks.com/enterprise-java/jms/apache-activemq-distributed-queue-tutorial/

### Project Name

```
cd C:\nc\Java-Apps\apache-activemq-5.16.0\bin
activemq.bat start
```
**Admin Console**  
URL: http://127.0.0.1:8161/admin/  
Login: admin  
Password: admin


---

```
cd C:\nc\Java-Architect\amazon-message-queues\AMQ-Michael-Heydt-Code1\src\main\java\in\connect2tech\mesh\inbuilt
javac -cp "C:\nc\Java-Architect\amazon-message-queues\AMQ-Michael-Heydt-Code1\jar-files\*" *.java

cd C:\nc\Java-Architect\amazon-message-queues\AMQ-Michael-Heydt-Code1\src\main\java\
java -cp "C:\nc\Java-Architect\amazon-message-queues\AMQ-Michael-Heydt-Code1\jar-files\*";. in.connect2tech.mesh.inbuilt.ProducerMeshInBuilt
```




