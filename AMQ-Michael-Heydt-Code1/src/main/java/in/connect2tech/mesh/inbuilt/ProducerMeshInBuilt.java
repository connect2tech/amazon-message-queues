package in.connect2tech.mesh.inbuilt;

import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.activemq.ActiveMQSslConnectionFactory;
import javax.jms.*;

public class ProducerMeshInBuilt {
	private final static String OPENWIRE_ENDPOINT = "failover:(ssl://b-70d3b4cd-0496-47fd-b158-1170ad214dcb-1.mq.ap-southeast-1.amazonaws.com:61617,ssl://b-25b62b9c-59be-483d-8da8-0f66d82d4153-1.mq.ap-southeast-1.amazonaws.com:61617,ssl://b-33383ba4-0e76-451f-b050-492c26b8fb8e-1.mq.ap-southeast-1.amazonaws.com:61617)";
	//private final static String OPENWIRE_ENDPOINT = "ssl://b-70d3b4cd-0496-47fd-b158-1170ad214dcb-1.mq.ap-southeast-1.amazonaws.com:61617";
	private final static String ACTIVE_MQ_USERNAME = "awsmq";
	private final static String ACTIVE_MQ_PASSWORD = "Aws1Broker1aa3";
	private final static String QUEUE_NAME = "MyQueue";

	public static void main(String[] args) {
		// write your code here
		try {
			Connection connection = getConnection();
			produce(connection);
			connection.close();

			System.out.println("Exiting app");
		} catch (JMSException e) {
			System.out.println(e.getMessage());
		}
	}

	private static Connection getConnection() throws JMSException {
		// Create a connection factory.
		final PooledConnectionFactory connectionFactory = createConnectionFactory();
		Connection connection = connectionFactory.createConnection();

		connection.start();

		return connection;
	}

	private static PooledConnectionFactory createConnectionFactory() {
		// Create a connection factory.
		final ActiveMQSslConnectionFactory connectionFactory = new ActiveMQSslConnectionFactory(OPENWIRE_ENDPOINT);

		// Pass the username and password.
		connectionFactory.setUserName(ACTIVE_MQ_USERNAME);
		connectionFactory.setPassword(ACTIVE_MQ_PASSWORD);

		final PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
		pooledConnectionFactory.setConnectionFactory(connectionFactory);
		pooledConnectionFactory.setMaxConnections(10);
		return pooledConnectionFactory;
	}

	private static void produce(Connection connection) throws JMSException {
		final Session producerSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		final Destination producerDestination = producerSession.createQueue(QUEUE_NAME);
		final MessageProducer producer = producerSession.createProducer(producerDestination);
		producer.setDeliveryMode(DeliveryMode.PERSISTENT);

		for (int i = 0; i < 100; i++) {
			
			
			
			final String text = "Welcome to MESH" + i;

			try {
				Thread.sleep(1000 * 3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			final TextMessage producerMessage = producerSession.createTextMessage(text);

			producer.send(producerMessage);
			System.out.println("Message sent." + i);
		}

		producer.close();
		producerSession.close();
	}

}
