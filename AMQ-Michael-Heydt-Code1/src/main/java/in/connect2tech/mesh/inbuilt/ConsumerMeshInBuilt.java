package in.connect2tech.mesh.inbuilt;

import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.activemq.ActiveMQSslConnectionFactory;
import javax.jms.*;

public class ConsumerMeshInBuilt {
	private final static String OPENWIRE_ENDPOINT = "failover:(ssl://b-70d3b4cd-0496-47fd-b158-1170ad214dcb-1.mq.ap-southeast-1.amazonaws.com:61617,ssl://b-25b62b9c-59be-483d-8da8-0f66d82d4153-1.mq.ap-southeast-1.amazonaws.com:61617,ssl://b-33383ba4-0e76-451f-b050-492c26b8fb8e-1.mq.ap-southeast-1.amazonaws.com:61617)";
	private final static String ACTIVE_MQ_USERNAME = "awsmq";
	private final static String ACTIVE_MQ_PASSWORD = "Aws1Broker1aa3";
	private final static String QUEUE_NAME = "MyQueue";

	public static void main(String[] args) {
		// write your code here
		try {
			Connection connection = getConnection();
			//produce(connection);
			consume(connection);
			connection.close();

			System.out.println("Exiting app");
		} catch (JMSException e) {
			System.out.println(e.getMessage());
		}
	}

	private static Connection getConnection() throws JMSException {
		// Create a connection factory.
		final PooledConnectionFactory connectionFactory = createConnectionFactory();
		Connection connection = connectionFactory.createConnection();

		connection.start();

		return connection;
	}

	private static PooledConnectionFactory createConnectionFactory() {
		// Create a connection factory.
		final ActiveMQSslConnectionFactory connectionFactory = new ActiveMQSslConnectionFactory(OPENWIRE_ENDPOINT);

		// Pass the username and password.
		connectionFactory.setUserName(ACTIVE_MQ_USERNAME);
		connectionFactory.setPassword(ACTIVE_MQ_PASSWORD);

		final PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
		pooledConnectionFactory.setConnectionFactory(connectionFactory);
		pooledConnectionFactory.setMaxConnections(10);
		return pooledConnectionFactory;
	}

	private static void consume(Connection connection) throws JMSException {
		final Session consumerSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		final Destination consumerDestination = consumerSession.createQueue(QUEUE_NAME);
		final MessageConsumer consumer = consumerSession.createConsumer(consumerDestination);

		int i = 0;
		while (i++ < 1000) {
			final Message consumedMessage = consumer.receive(1000);
			try {
				Thread.sleep(1000*3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (consumedMessage != null) {
				final TextMessage consumedTextMessage = (TextMessage) consumedMessage;
				System.out.println(consumedTextMessage.getText());
			} else {
				System.out.println("No message received");
			}
		}

		System.out.println("Closing consumer");
		consumer.close();
		consumerSession.close();
	}
}
