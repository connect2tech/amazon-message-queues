package jcg.demo.util;

/**
 * The input data for this demo.
 * 
 * @author Mary.Zheng
 *
 */
public class InputData {

	private String brokerUrl;
	private String queueName;

	public InputData(String brokerUrl, String queueName) {
		super();
		this.brokerUrl = brokerUrl.trim();
		this.queueName = queueName.trim();
	}

	public String getBrokerUrl() {
		return brokerUrl;
	}

	public void setBrokerUrl(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
}
