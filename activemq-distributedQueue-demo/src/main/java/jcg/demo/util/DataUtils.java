package jcg.demo.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import org.springframework.util.StringUtils;

/**
 * The data utility used in this Demo
 * 
 * @author Mary.Zheng
 *
 */
public final class DataUtils {

	private static final String INPUT_PROMPT_1 = "Enter Broker URL(tcp://$host:$port): ";
	private static final String INPUT_PROMPT_2 = "Enter Queue Name: ";
	public static final int MESSAGE_SIZE = 10;
	public static final String ADMIN = "admin";

	public static String buildDummyMessage(int value) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();		
		
		return "dummy message [" + value + "], created at " + dtf.format(now);
	}

	public static InputData readTestData() {
		InputData testData = null;

		try (Scanner scanIn = new Scanner(System.in)) {		
			System.out.println(INPUT_PROMPT_1);
			String brokerUrl = scanIn.nextLine();

			System.out.println(INPUT_PROMPT_2);
			String queueName = scanIn.nextLine();
			
			if (StringUtils.isEmpty(queueName) || StringUtils.isEmpty(brokerUrl)) {
				return testData;
			}
			testData = new InputData( brokerUrl, queueName);
		}

		return testData;
	}
}
