package jcg.demo.activemq;

import javax.jms.JMSException;

import jcg.demo.util.DataUtils;
import jcg.demo.util.InputData;

public class MessageConsumerApp {

	public static void main(String[] args) {

		InputData brokerTestData = DataUtils.readTestData();
		if (brokerTestData == null) {
			System.out.println("Wrong input");
		} else {
			QueueMessageConsumer queueMsgListener = new QueueMessageConsumer(brokerTestData.getBrokerUrl(), DataUtils.ADMIN,
					DataUtils.ADMIN);
			queueMsgListener.setDestinationName(brokerTestData.getQueueName());

			try {
				queueMsgListener.run();
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}
}
