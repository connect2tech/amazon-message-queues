package jcg.demo.activemq;

import jcg.demo.util.DataUtils;
import jcg.demo.util.InputData;

public class MessageProducerApp {

	public static void main(String[] args) {
		InputData brokerTestData = DataUtils.readTestData();

		if (brokerTestData == null) {
			System.out.println("Wrong input");
		} else {
			QueueMessageProducer queProducer = new QueueMessageProducer(brokerTestData.getBrokerUrl(), DataUtils.ADMIN,
					DataUtils.ADMIN);
			queProducer.sendDummyMessages(brokerTestData.getQueueName());
		}
	}
}
